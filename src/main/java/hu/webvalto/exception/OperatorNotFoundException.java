package hu.webvalto.exception;

/**
 * @author Attila Budai <attila.budai@webvalto.hu>
 */
public class OperatorNotFoundException extends Exception {

    private static final String exceptionMessage = "Operator not found: %c";

    public OperatorNotFoundException(Character operator) {
        super(String.format(exceptionMessage, operator));
    }

}
