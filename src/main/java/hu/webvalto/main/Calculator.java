package hu.webvalto.main;

import hu.webvalto.exception.OperatorNotFoundException;

/**
 * @author Attila Budai <attila.budai@webvalto.hu>
 */
public class Calculator {

    public int calculate(Integer valueA, Character operator, Integer valueB) throws OperatorNotFoundException {
        switch (operator) {
            case '+':
                return valueA + valueB;
            case '-':
                return valueA - valueB;
            default:
                throw new OperatorNotFoundException(operator);
        }
    }

}
